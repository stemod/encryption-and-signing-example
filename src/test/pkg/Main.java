package test.pkg;

import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;


public class Main {
    private static Charset charset = StandardCharsets.UTF_8;
    private static String licence = "{\"devices\": 30, \"name\": \"any guy\"}";
    private static String encrypted = "+qN+YyZkyJ8PaTquSLz7uw==$pM6Rewh7V4FCiIeT+UNpsU599NPR3Cs0DjrHlZvZVUMMethUIRgUxkC+uhAzN3TQ";
    private static String signed = "l/pgY3E6x6ePCQ0q8mOUfA==$oAFQDInjm7yXBTS1XQng0OE5d3xwgXdMsSU8ssDtV0FNurdRec8V1tVawj4GB1CH$RjuReuByccqHgDwujWgAJo0h/meWmJ5ZiBQL/VX26r1fXoUmIXPPZNTn+AjlRFBfMfHMTlL8SyFlvbMQbP9dK2+ogyKT/NXnRi9/irwieQQARD7dZQyeEtft+tsqwJxBvOjCLUnf3JHRyuuBZ69QQldNU93vj5lReNa+bR00434l3HDWuuZfvD+rMkWMPi8VVjxnQhOrTrHoIX/NL5QUEf+cNiIvTdWAyvCj06wXWGMdG9Oqfs98J98n+fv2C0XRnEV8E/aF/HUp7zrJ2AwjcWBaAqUK9kf46wF7URFcuJ0AaXB2SL81JZ/mWLTJVojbkC0Hds+78CPIUxfYvNn8/GPN+NzoiClWpFmdhFuS2wcz1Tpvr/XVVZZmeOry0ZoYf0T9+mlP3AiGr9PZAzA3vt+gqq4O9XooJs7hLGmH0LWYfh6/h8zryPEXjqczrvsRj2bbAWkff5RhFzLlk/7l6P8WUF+twQ3UkY7lJ9Z/a2V2ewibRcY6Pzyg9h0+du7RzuQjubIholdrlauTk+SaB6/njRS6hmCZp+Rx/0NrT3wh/ASiik4zhirQuKspY4/5FVPgY4B9VkT3HXdvZ9ZKMhP5U2YWg7xwRSzpIS7hhw8UjwxR+HSPmY/cfIRxKstATyb7+U909cyVBBVk1jZ/4REOR37J9wJLH70HImjoLLU=";

    public static void main(String[] args) {

        for (String arg: args) {
            switch (arg) {
                case "generate":
                    KeyPair keyPair = generateKeyPair();

                    try {
                        String privateKey = new BASE64Encoder().encode(keyPair.getPrivate().getEncoded());
                        BufferedWriter writer = new BufferedWriter(new FileWriter("private.key"));
                        writer.write(privateKey);
                        writer.close();

                        String publicKey = new BASE64Encoder().encode(keyPair.getPublic().getEncoded());
                        writer = new BufferedWriter(new FileWriter("public.key"));
                        writer.write(publicKey);
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "encrypt":
                    String encrypted = encrypt();
                    System.out.println(encrypted);
                    break;

                case "decrypt":
                    String decrypted = decrypt();
                    System.out.println(decrypted);
                    break;
                case "sign":
                    String signed = sign();
                    System.out.println(signed);
                    break;
                case "verify":
                    verify();
                default:
                    break;
            }
        }

        System.out.println(Arrays.toString(args));
    }

    private static KeyPair generateKeyPair() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(4096);
            return  keyPairGenerator.genKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String encrypt() {
        try {
            String tmpKey = Main.generateRandomString();
            byte[] key = tmpKey.getBytes(charset);
            MessageDigest sha = MessageDigest.getInstance("SHA-512");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            return Base64.getEncoder().encodeToString(key) + "$"
                    + Base64.getEncoder().encodeToString(cipher.doFinal(licence.getBytes(charset)));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String decrypt()
    {
        try {
            String[] parts = encrypted.split("\\$");

            byte[] key = Base64.getDecoder().decode(parts[0]);
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            return new String(cipher.doFinal(Base64.getDecoder().decode(parts[1])));
        }
        catch (Exception e)
        {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }

    private static String sign() {
        try {
            File privKeyFile = new File("private.key");

            String privateKeyContent = new String(Files.readAllBytes(Paths.get(privKeyFile.toURI())));

            privateKeyContent = privateKeyContent.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");

            KeyFactory kf = KeyFactory.getInstance("RSA");

            PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
            PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);

            String encrypted = encrypt();
            byte[] data = encrypted.getBytes(charset);

            Signature sig = Signature.getInstance("SHA1WithRSA");
            sig.initSign(privKey);
            sig.update(data);
            byte[] signatureBytes = sig.sign();

            String signed = encrypted + "$" + new BASE64Encoder().encode(signatureBytes).replaceAll("\\n", "");
            System.out.println("Signed:" + signed);

            return signed;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void verify() {
        try {
            String[] parts = signed.split("\\$");
            byte[] signature = Base64.getDecoder().decode(parts[2]);

            File pubKeyFile = new File("public.key");
            String publicKeyContent = new String(Files.readAllBytes(Paths.get(pubKeyFile.toURI())));
            publicKeyContent = publicKeyContent.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");

            KeyFactory kf = KeyFactory.getInstance("RSA");

            X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
            RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

            Signature sig = Signature.getInstance("SHA1WithRSA");
            String testData = parts[0] + "$" + parts[1];
            byte[] data = testData.getBytes(charset);

            sig.initVerify(pubKey);
            sig.update(data);

            System.out.println(sig.verify(signature));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String generateRandomString() {
        byte[] array = new byte[16];
        new Random().nextBytes(array);

        return new String(array, charset);
    }
}


